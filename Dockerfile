# postgres
docker run -d --name rss-postgres \
 --net=rss \
 -e POSTGRES_DB=audioboom \
 -e POSTGRES_PASSWORD=parserssfeed \
 -p 5432:5432 \
 -v $(PWD):/home \
 -w /home  \
 postgres

# Python
docker run -dt --name rss-python \
 --net=rss \
 -v $(PWD)/src:/home/src \
 -w /home/src  \
 conda/miniconda3-centos6 bash